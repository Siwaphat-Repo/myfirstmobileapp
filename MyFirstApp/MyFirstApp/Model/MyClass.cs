﻿using MyFirstApp.Model.Interface;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Text;

namespace MyFirstApp.Model
{
    public class MyClass : IMyInterface
    {
        public MyClass()
        {
            MyName = "Testing something.";
            compositeCommand.Execute(null);
        }

        private string _MyName;
        public string MyName
        {
            get { return _MyName; }
            set { _MyName = value; }
        }

        public CompositeCommand compositeCommand = new CompositeCommand();

    }
}
