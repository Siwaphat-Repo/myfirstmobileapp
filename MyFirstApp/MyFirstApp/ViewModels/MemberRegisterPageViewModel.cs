﻿using MyFirstApp.Model.Interface;
using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyFirstApp.ViewModels
{
    public class MemberRegisterPageViewModel : BindableBase
    {
        private readonly IPageDialogService pageDialogService;
        private readonly INavigationService navigationService;
        private readonly IMyInterface myInterface;

        public MemberRegisterPageViewModel(IPageDialogService pageDialogService, INavigationService navigationService, IMyInterface myInterface)
        {
            this.pageDialogService = pageDialogService;
            this.navigationService = navigationService;
            this.myInterface = myInterface;
        }

        #region Properties
        private string _Name;
        public string Name
        {
            get { return _Name; }
            set
            {
                SetProperty(ref _Name, value);
                RaisePropertyChanged("FullName");
                RaisePropertyChanged("IsEnableRegister");
            }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                SetProperty(ref _LastName, value);
                RaisePropertyChanged("FullName");
                RaisePropertyChanged("IsEnableRegister");
            }
        }

        public string FullName
        {
            get
            {
                try
                {
                    return string.Format("{0} {1}", Name, LastName);
                }
                catch
                {
                    return string.Empty;
                }

            }
        }

        //private bool _IsEnableRegister;

        //public bool IsEnableRegister
        //{
        //    get { return _IsEnableRegister; }
        //    set { SetProperty(ref _IsEnableRegister, value); }
        //}

        public bool IsEnableRegister
        {
            get
            {
                if (!string.IsNullOrEmpty(Name) && !string.IsNullOrEmpty(LastName))
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        #region Commands and its methods
        private DelegateCommand _CMDRegister;
        public DelegateCommand CMDRegister =>
             _CMDRegister ?? new DelegateCommand(MemberRegister).ObservesCanExecute(() => IsEnableRegister);

        public void MemberRegister()
        {
            string something = "Something here.";
        }

        bool CanRegister()
        {
            //return await pageDialogService.DisplayAlertAsync("Confirm?", "Do you want to register member.", "Yes", "No");
            return true;
        }


        private DelegateCommand<string> _CMDRegisterWithPrm;
        public DelegateCommand<string> CMDRegisterWithPrm =>
            _CMDRegisterWithPrm ?? new DelegateCommand<string>(MemberRegister);

        public void MemberRegister(string Name)
        {

        }
        #endregion
    }
}
