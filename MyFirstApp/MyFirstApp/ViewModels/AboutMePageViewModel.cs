﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyFirstApp.ViewModels
{
	public class AboutMePageViewModel : BindableBase
	{
        public AboutMePageViewModel()
        {
            AboutMe = "The main purpose of this app is for practicing programming with MVVM concept and create an application from Xamarin framework.";
        }

        #region Properties
        private string _AboutMe;
        public string AboutMe
        {
            get { return _AboutMe; }
            set { _AboutMe = value; }
        }
        #endregion
    }
}
