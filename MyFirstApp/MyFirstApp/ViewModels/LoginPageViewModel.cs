﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyFirstApp.ViewModels
{
	public class LoginPageViewModel : BindableBase
	{
        private readonly INavigationService navigationService;
        private readonly IPageDialogService dialogService;

        public LoginPageViewModel(INavigationService navigationService, IPageDialogService dialogService)
        {
            this.navigationService = navigationService;
            this.dialogService = dialogService;
        }

        #region Properties
        private string _Username;
        public string Username
        {
            get { return _Username; }
            set { SetProperty(ref _Username, value); }
        }

        private string _Password;
        public string Password
        {
            get { return _Password; }
            set { SetProperty(ref _Password, value); }
        }
        
        #endregion

        #region Commands and methods
        //Login.
        private DelegateCommand _Cmd_Login;
        public DelegateCommand Cmd_Login =>
            _Cmd_Login ?? new DelegateCommand(Login);
         
        public async void Login()
        {
            await navigationService.NavigateAsync("AboutMePage");
        }

        
        #endregion

         
    }
}
