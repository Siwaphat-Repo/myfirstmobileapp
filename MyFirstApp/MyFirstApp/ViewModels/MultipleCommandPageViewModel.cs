﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MyFirstApp.ViewModels
{
    public class MultipleCommandPageViewModel : BindableBase
    {
        private readonly IPageDialogService pageDialogService;

        public MultipleCommandPageViewModel(IPageDialogService pageDialogService)
        {
            CMD_A = new DelegateCommand(MethodA);
            CMD_B = new DelegateCommand(MethodB);
            CMD_C = new DelegateCommand(MethodC);
            AllCMD = new CompositeCommand();
            AllCMD.RegisterCommand(CMD_A);
            AllCMD.RegisterCommand(CMD_B);
            AllCMD.RegisterCommand(CMD_C);
            this.pageDialogService = pageDialogService;
        }

        #region Commands
        public DelegateCommand CMD_A { get; set; }
        public DelegateCommand CMD_B { get; set; }
        public DelegateCommand CMD_C { get; set; }
        public CompositeCommand AllCMD { get; set; }
        #endregion

        #region Methods
        public async void MethodA()
        {
            await pageDialogService.DisplayAlertAsync("Alert A", "Alert A", "Something");
        }

        public async void MethodB()
        {
            await pageDialogService.DisplayAlertAsync("Alert B", "Alert B", "Something");
        }

        public async void MethodC()
        {
            await pageDialogService.DisplayAlertAsync("Alert C", "Alert C", "Something");
        }


        #endregion
    }
}
