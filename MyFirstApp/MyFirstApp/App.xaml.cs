﻿using Prism;
using Prism.Ioc;
using MyFirstApp.ViewModels;
using MyFirstApp.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using MyFirstApp.Model.Interface;
using MyFirstApp.Model;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace MyFirstApp
{
    public partial class App
    {
        /* 
         * The Xamarin Forms XAML Previewer in Visual Studio uses System.Activator.CreateInstance.
         * This imposes a limitation in which the App class must have a default constructor. 
         * App(IPlatformInitializer initializer = null) cannot be handled by the Activator.
         */
        public App() : this(null) { }

        public App(IPlatformInitializer initializer) : base(initializer) { }

        protected override async void OnInitialized()
        {
            InitializeComponent();
            await NavigationService.NavigateAsync("NavigationPage/MultipleCommandPage");
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterSingleton<IMyInterface, MyClass>();

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainPage, MainPageViewModel>();
            containerRegistry.RegisterForNavigation<LoginPage, LoginPageViewModel>();
            containerRegistry.RegisterForNavigation<AboutMePage, AboutMePageViewModel>();
            containerRegistry.RegisterForNavigation<MainMasterDetailPage, MainMasterDetailPageViewModel>();
            containerRegistry.RegisterForNavigation<MemberRegisterPage, MemberRegisterPageViewModel>();
            containerRegistry.RegisterForNavigation<MultipleCommandPage, MultipleCommandPageViewModel>();
        }
    }
}
